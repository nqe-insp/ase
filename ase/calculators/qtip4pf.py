import numpy as np

from ase import units
from ase.calculators.calculator import Calculator, all_changes
from ase.calculators.tip3p import rOH, angleHOH, TIP3P
from ase.calculators.tip4p import TIP4P

__all__ = ['rOH', 'angleHOH', 'TIP4P', 'sigma0', 'epsilon0']

# Electrostatic constant and parameters:
k_c = units.Hartree * units.Bohr
qH = 0.5564
sigma0 = 5.96946 * units.Bohr 
epsilon0 = 2.95147e-4
reOH =  1.779933 * units.Bohr
theta = 107.4 * np.pi / 180.
alpha = 0.73612 
alpha2= 0.5*(1.-alpha)
alp = 1.21 / units.Bohr
Estretch = 0.185*units.Hartree
Ebend = 0.07*units.Hartree
#https://doi.org/10.1063/1.3167790


class QTIP4PF(TIP4P):
    def __init__(self, rc=9.0, width=1.0):
        """ QTIP4P-F flexible potential for water.

        https://doi.org/10.1063/1.3167790

        Requires an atoms object of OHH,OHH, ... sequence
        Correct TIP4P charges and LJ parameters set automatically.

        Virtual interaction sites implemented in the following scheme:
        Original atoms object has no virtual sites.
        When energy/forces are requested:

        * virtual sites added to temporary xatoms object
        * energy / forces calculated
        * forces redistributed from virtual sites to actual atoms object

        This means you do not get into trouble when propagating your system
        with MD while having to skip / account for massless virtual sites.

        This also means that if using for QM/MM MD with GPAW, the EmbedTIP4P
        class must be used.
        """

        TIP4P.__init__(self, rc, width)

    def calculate(self, atoms=None,
                  properties=['energy', 'forces','charges'],
                  system_changes=all_changes):
        Calculator.calculate(self, atoms, properties, system_changes)

        assert (atoms.numbers[::3] == 8).all()
        assert (atoms.numbers[1::3] == 1).all()
        assert (atoms.numbers[2::3] == 1).all()

        xpos = self.add_virtual_sites(atoms.positions)
        xcharges = self.get_virtual_charges(atoms)

        cell = atoms.cell
        pbc = atoms.pbc

        natoms = len(atoms)
        nmol = natoms // 3

        self.charges=np.zeros(natoms)

        self.energy = 0.0
        self.forces = np.zeros((4 * natoms // 3, 3))

        C = cell.diagonal()
        assert (cell == np.diag(C)).all(), 'not orthorhombic'
        assert ((C >= 2 * self.rc) | ~pbc).all(), 'cutoff too large'

        # Get dx,dy,dz from first atom of each mol to same atom of all other
        # and find min. distance. Everything moves according to this analysis.
        for a in range(nmol - 1):
            D = xpos[(a + 1) * 4::4] - xpos[a * 4]
            shift = np.zeros_like(D)
            for i, periodic in enumerate(pbc):
                if periodic:
                    shift[:, i] = np.rint(D[:, i] / C[i]) * C[i]
            q_v = xcharges[(a + 1) * 4:]

            # Min. img. position list as seen for molecule !a!
            position_list = np.zeros(((nmol - 1 - a) * 4, 3))

            for j in range(4):
                position_list[j::4] += xpos[(a + 1) * 4 + j::4] - shift

            # Make the smooth cutoff:
            pbcRoo = position_list[::4] - xpos[a * 4]
            pbcDoo = np.sum(np.abs(pbcRoo)**2, axis=-1)**(1 / 2)
            x1 = pbcDoo > self.rc - self.width
            x2 = pbcDoo < self.rc
            x12 = np.logical_and(x1, x2)
            y = (pbcDoo[x12] - self.rc + self.width) / self.width
            t = np.zeros(len(pbcDoo))
            t[x2] = 1.0
            t[x12] -= y**2 * (3.0 - 2.0 * y)
            dtdd = np.zeros(len(pbcDoo))
            dtdd[x12] -= 6.0 / self.width * y * (1.0 - y)
            self.energy_and_forces(a, xpos, position_list, q_v, nmol, t, dtdd)

        if self.pcpot:
            e, f = self.pcpot.calculate(xcharges, xpos)
            self.energy += e
            self.forces += f

        f = self.redistribute_forces(self.forces,xpos)
        
        eintra,fintra = self.intra_morse_harm(atoms.positions)
        self.energy+=eintra

        self.results['energy'] = self.energy
        self.results['forces'] = f+fintra

    def energy_and_forces(self, a, xpos, position_list, q_v, nmol, t, dtdd):
        """ energy and forces on molecule a from all other molecules.
            cutoff is based on O-O Distance. """

        # LJ part - only O-O interactions
        epsil = np.tile([epsilon0], nmol - 1 - a)
        sigma = np.tile([sigma0], nmol - 1 - a)
        DOO = position_list[::4] - xpos[a * 4]
        d2 = (DOO**2).sum(1)
        d = np.sqrt(d2)
        e_lj = 4 * epsil * (sigma**12 / d**12 - sigma**6 / d**6)
        f_lj = (4 * epsil * (12 * sigma**12 / d**13 -
                             6 * sigma**6 / d**7) * t -
                e_lj * dtdd)[:, np.newaxis] * DOO / d[:, np.newaxis]

        self.forces[a * 4] -= f_lj.sum(0)
        self.forces[(a + 1) * 4::4] += f_lj

        # Electrostatics
        e_elec = 0
        all_cut = np.repeat(t, 4)
        for i in range(4):
            D = position_list - xpos[a * 4 + i]
            d2_all = (D**2).sum(axis=1)
            d_all = np.sqrt(d2_all)
            e = k_c * q_v[i] * q_v / d_all
            e_elec += np.dot(all_cut, e).sum()
            e_f = e.reshape(nmol - a - 1, 4).sum(1)
            F = (e / d_all * all_cut)[:, np.newaxis] * D / d_all[:, np.newaxis]
            FOO = -(e_f * dtdd)[:, np.newaxis] * DOO / d[:, np.newaxis]
            self.forces[(a + 1) * 4 + 0::4] += FOO
            self.forces[a * 4] -= FOO.sum(0)
            self.forces[(a + 1) * 4:] += F
            self.forces[a * 4 + i] -= F.sum(0)

        self.energy += np.dot(e_lj, t) + e_elec

    def add_virtual_sites(self, pos):
        # Order: OHHM,OHHM,...
        # DOI: 10.1002/(SICI)1096-987X(199906)20:8
        xatomspos = np.zeros((4 * len(pos) // 3, 3))
        for w in range(0, len(pos), 3):
            r_i = pos[w]  # O pos
            r_j = pos[w + 1]  # H1 pos
            r_k = pos[w + 2]  # H2 pos
            r_d = alpha*r_i + alpha2*(r_j+r_k) 

            x = 4 * w // 3
            xatomspos[x + 0] = r_i
            xatomspos[x + 1] = r_j
            xatomspos[x + 2] = r_k
            xatomspos[x + 3] = r_d

        return xatomspos

    def get_virtual_charges(self, atoms):
        charges = np.empty(len(atoms) * 4 // 3)
        charges[0::4] = 0.00  # O
        charges[1::4] = qH  # H1
        charges[2::4] = qH  # H2
        charges[3::4] = - 2 * qH  # X1
        return charges

    def redistribute_forces(self, forces, xpos):
        f = forces
        for w in range(0, len(xpos), 4):
            r_i = xpos[w]  # O pos
            r_j = xpos[w + 1]  # H1 pos
            r_k = xpos[w + 2]  # H2 pos
            r_d = xpos[w + 3] 
            f[w] += alpha*f[w+3] 
            f[w + 1] += alpha2*f[w+3] 
            f[w + 2] += alpha2*f[w+3]

        # remove virtual sites from force array
        f = np.delete(f, list(range(3, f.shape[0], 4)), axis=0)
        return f
    
    def intra_morse_harm(self,pos):
        e=0
        de=np.zeros_like(pos)
        np.arange(0,len(pos),3)
        for w in range(0,len(pos),3):
            rOH1=pos[w+1,:]-pos[w,:]
            rOH2=pos[w+2,:]-pos[w,:]
            rH1H2=pos[w+2,:]-pos[w+1,:]
            d1=np.linalg.norm(rOH1)
            d2=np.linalg.norm(rOH2)
            d3=np.linalg.norm(rH1H2)
            rOH1/=d1
            rOH2/=d2
            rH1H2/=d3
            # Stretching
            rr1 = (d1 - reOH)*alp
            rr2 = (d2 - reOH)*alp
            e += Estretch*(rr1**2 - rr1**3 +(7./12.)*rr1**4)
            e += Estretch*(rr2**2 - rr2**3 +(7./12.)*rr2**4)

            a1 = Estretch*alp*(2.*rr1 - 3.*rr1**2 + (7./3.)*rr1**3)
            a2 = Estretch*alp*(2.*rr2 - 3.*rr2**2 + (7./3.)*rr2**3)

            # Bending
            u = d1**2 + d2**2 - d3**2
            vv = 2*d1*d2
            arg=u/vv
            darg = -1./np.sqrt(1. - arg**2)
            dthetadr1=darg*2.*(d1*vv - d2*u)/vv**2
            dthetadr2=darg*2.*(d2*vv - d1*u)/vv**2
            dthetadr3=-darg*2.*d3/vv
            dang=np.arccos(arg) - theta
            #print(d1,d2,d3,np.arccos(u/vv)*180./np.pi)

            e += Ebend*dang**2
            a3 = 2.*Ebend*dang

            # project derivatives 
            dvdr1=a1+a3*dthetadr1
            dvdr2=a2+a3*dthetadr2
            dvdr3=a3*dthetadr3
            de[w,:]   +=  -dvdr1*rOH1 - dvdr2*rOH2
            de[w+1,:] +=   dvdr1*rOH1 - dvdr3*rH1H2
            de[w+2,:] +=   dvdr2*rOH2 + dvdr3*rH1H2

        return e,-de

